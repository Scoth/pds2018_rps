<?php
require_once 'dbconnect.php';

if (isset($_GET['edit_id']) && !empty($_GET['edit_id'])) {

    $id = $_GET['edit_id'];
    $select_edit = mysql_query("SELECT * FROM investimentos WHERE id_investimentos =" . $id);

    $row = mysql_fetch_array($select_edit);

    $nome = $row['nome_investimento']; 
    $valor = $row['valor']; 
    $juros = $row['juros'];

    //echo '<pre style="padding-top:150px;">';
    //    var_dump($row['apelido']);
    //echo '</pre>';
    
} else {
    header("Location: investimentos.php");
}

$res = mysql_query("SELECT * FROM usuarios WHERE id_usuarios=" . $row['id_usuarios']);
$userRow = mysql_fetch_array($res);

if (isset($_POST['btn_save_updates'])) {

    $nome2 = $_POST['nome']; 
    $valor2 = $_POST['valor']; 
    $juros2 = $_POST['juros'];

    $stmt = mysql_query("UPDATE investimentos
                                    SET nome_investimento='" . $nome2 . "',
                                        valor='" . $valor2 . "',
                                        juros='" . $juros2 . "'
                              WHERE id_investimentos=" . $id);
    ?>
    <script>
        alert('Atualizado com sucesso ...');
        window.location.href = 'investimentos.php';
    </script>
    <?php
}
?>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Alterar Investimentos</title>
        <link rel="stylesheet" href="assets/css/bootstrap.min.css" type="text/css"  />
        <link rel="stylesheet" href="style.css" type="text/css" />
        <script>
    var myChart = new Chart({...})
        </script>
    </head>
    <body>

        <nav class="navbar navbar-default navbar-fixed-top">
         <div class="container">
            <div class="navbar-header">
               <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
               <span class="sr-only">Toggle navigation</span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               </button>
               <a class="navbar-brand" href="home.php">FinanceOne</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
               <ul class="nav navbar-nav">
                  <li><a href="home.php">Dashboard</a></li>
                  <li><a href="contas_pagar.php">Contas a Pagar</a></li>
                  <li><a href="contas_receber.php">Contas a Receber</a></li>
                  <li class="active"><a href="investimentos.php">Investimentos</a></li>
               </ul>
               <ul class="nav navbar-nav navbar-right">
                  <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                     <span class="glyphicon glyphicon-user"></span>&nbsp;Ol&aacute; <?php echo $userRow['usuario']; ?>&nbsp;<span class="caret"></span></a>
                     <ul class="dropdown-menu">
                        <li><a href="logout.php?logout"><span class="glyphicon glyphicon-log-out"></span>&nbsp;Sair</a></li>
                     </ul>
                  </li>
               </ul>
            </div>
         </div>
		</nav>

        <div id="wrapper" align="center">
            <div class="container" style="width: 30%">


                <div class="page-header">
                    <h1 class="h2">Alterar Investimento <a class="btn btn-default" href="investimentos.php"> Voltar </a></h1>
                </div>

                <form method="post" enctype="multipart/form-data" class="form-horizontal">


                    <?php
                    if (isset($errMSG)) {
                        ?>
                        <div class="alert alert-danger">
                            <span class="glyphicon glyphicon-info-sign"></span> &nbsp; <?php echo $errMSG; ?>
                        </div>
                        <?php
                    }
                    ?>


                    <table class="table table-bordered table-responsive">

                        <tr>
                            <td><label class="control-label">Nome Investimento:</label></td>
                            <td><input class="form-control" type="text" name="nome" value="<?php echo $nome; ?>" required /></td>
                        </tr>
						<tr>
                            <td><label class="control-label">Valor:</label></td>
                            <td><input class="form-control" type="text" name="valor" value="<?php echo $valor; ?>" required /></td>
                        </tr>
                        <tr>
                            <td><label class="control-label">Juros ao mês:</label></td>
                            <td><input class="form-control" type="text" name="juros" value="<?php echo $juros; ?>" required /></td>
                        </tr>

                        <tr>
                            <td colspan="2">
                                <button type="submit" name="btn_save_updates" class="btn btn-default">
                                    <span class="glyphicon glyphicon-save"></span> Editar
                                </button>

                                <a class="btn btn-default" href="investimentos.php"> <span class="glyphicon glyphicon-backward"></span> Cancelar </a>

                            </td>
                        </tr>

                    </table>

                </form>

            </div>
        </div>
    </body>
</html>