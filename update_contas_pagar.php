<?php
require_once 'dbconnect.php';

if (isset($_GET['edit_id']) && !empty($_GET['edit_id'])) {

    $id = $_GET['edit_id'];
    $select_edit = mysql_query("SELECT * FROM contas_pagar WHERE id_contas_pagar =" . $id);

    $row = mysql_fetch_array($select_edit);

    $apelido = $row['apelido']; 
    $valor = $row['valor']; 
    $tipo = $row['tipo'];
    $forma_pagamento = $row['forma_pagamento'];
    $data = $row['data'];

    //echo '<pre style="padding-top:150px;">';
    //    var_dump($row['apelido']);
    //echo '</pre>';
    
} else {
    header("Location: contas_pagar.php");
}

$res = mysql_query("SELECT * FROM usuarios WHERE id_usuarios=" . $row['id_usuarios']);
$userRow = mysql_fetch_array($res);

if (isset($_POST['btn_save_updates'])) {

    $apelido2 = $_POST['apelido']; 
    $valor2 = $_POST['valor']; 
    $tipo2 = $_POST['tipo'];
    $forma_pagamento2 = $_POST['forma_pagamento'];
    $data2 = $_POST['data'];

    $stmt = mysql_query("UPDATE contas_pagar
                                    SET apelido='" . $apelido2 . "',
                                        valor='" . $valor2 . "',
                                        tipo='" . $tipo2 . "',
                                        forma_pagamento='" . $forma_pagamento2 . "',
                                        data='" . $data2 . "'
                              WHERE id_contas_pagar=" . $id);
    ?>
    <script>
        alert('Atualizado com sucesso ...');
        window.location.href = 'contas_pagar.php';
    </script>
    <?php
}
?>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Alterar Contas a Pagar</title>
        <link rel="stylesheet" href="assets/css/bootstrap.min.css" type="text/css"  />
        <link rel="stylesheet" href="style.css" type="text/css" />
        <script>
    var myChart = new Chart({...})
        </script>
        <link rel="stylesheet" type="text/css" media="screen" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" />
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
        <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
        <script src="https://igorescobar.github.io/jQuery-Mask-Plugin/js/jquery.mask.min.js"></script>  
    </head>
    <body>

        <nav class="navbar navbar-default navbar-fixed-top">
         <div class="container">
            <div class="navbar-header">
               <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
               <span class="sr-only">Toggle navigation</span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               </button>
               <a class="navbar-brand" href="home.php">FinanceOne</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
               <ul class="nav navbar-nav">
                  <li><a href="home.php">Dashboard</a></li>
                  <li class="active"><a href="contas_pagar.php">Contas a Pagar</a></li>
                  <li><a href="contas_receber.php">Contas a Receber</a></li>
                  <li><a href="investimentos.php">Investimentos</a></li>
               </ul>
               <ul class="nav navbar-nav navbar-right">
                  <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                     <span class="glyphicon glyphicon-user"></span>&nbsp;Ol&aacute; <?php echo $userRow['usuario']; ?>&nbsp;<span class="caret"></span></a>
                     <ul class="dropdown-menu">
                        <li><a href="logout.php?logout"><span class="glyphicon glyphicon-log-out"></span>&nbsp;Sair</a></li>
                     </ul>
                  </li>
               </ul>
            </div>
         </div>
		</nav>

        <div id="wrapper" align="center">
            <div class="container" style="width: 30%">


                <div class="page-header">
                    <h1 class="h2">Alterar Contas a Pagar <a class="btn btn-default" href="contas_pagar.php"> Voltar </a></h1>
                </div>

                <form method="post" enctype="multipart/form-data" class="form-horizontal">


                    <?php
                    if (isset($errMSG)) {
                        ?>
                        <div class="alert alert-danger">
                            <span class="glyphicon glyphicon-info-sign"></span> &nbsp; <?php echo $errMSG; ?>
                        </div>
                        <?php
                    }
                    ?>


                    <table class="table table-bordered table-responsive">

                        <tr>
                            <td><label class="control-label">Apelido:</label></td>
                            <td><input class="form-control" type="text" name="apelido" value="<?php echo $apelido; ?>" required /></td>
                        </tr>
						<tr>
                            <td><label class="control-label">Valor:</label></td>
                            <td><input class="form-control" type="text" name="valor" value="<?php echo $valor; ?>" required /></td>
                        </tr>

                        <tr>
                            <td><label class="control-label">Tipo de Conta:</label></td>
                            <td><select class="form-control"  name="tipo" required >
                                    <option><?php echo $tipo; ?></option>
                                        
                                    <?php if($tipo == 'Estudos') { ?>
                                    
                                        <option> <?php echo 'Comidas'; ?></option>
                                        <option> <?php echo 'Lazer'; ?></option>
                                        <option> <?php echo 'Transporte'; ?></option>
                                        <option> <?php echo 'Recarga Celular'; ?></option>
                                        <option> <?php echo 'Viagens'; ?></option>
                                        <option> <?php echo 'Outros'; ?></option>
                                        
                                    <?php } elseif($tipo == 'Comidas') { ?>
                                    
                                        <option> <?php echo 'Estudos'; ?></option>
                                        <option> <?php echo 'Lazer'; ?></option>
                                        <option> <?php echo 'Transporte'; ?></option>
                                        <option> <?php echo 'Recarga Celular'; ?></option>
                                        <option> <?php echo 'Viagens'; ?></option>
                                        <option> <?php echo 'Outros'; ?></option>
                                        
                                    <?php } elseif($tipo == 'Lazer') { ?>
                                    
                                        <option> <?php echo 'Estudos'; ?></option>
                                        <option> <?php echo 'Comidas'; ?></option>
                                        <option> <?php echo 'Transporte'; ?></option>
                                        <option> <?php echo 'Recarga Celular'; ?></option>
                                        <option> <?php echo 'Viagens'; ?></option>
                                        <option> <?php echo 'Outros'; ?></option>
										
                                    <?php } elseif($tipo == 'Transporte') { ?>
                                    
                                        <option> <?php echo 'Estudos'; ?></option>
                                        <option> <?php echo 'Lazer'; ?></option>
                                        <option> <?php echo 'Comidas'; ?></option>
                                        <option> <?php echo 'Recarga Celular'; ?></option>
                                        <option> <?php echo 'Viagens'; ?></option>
                                        <option> <?php echo 'Outros'; ?></option>
                                        
                                    <?php } elseif($tipo == 'Recarga Celular') { ?>
                                    
                                        <option> <?php echo 'Estudos'; ?></option>
                                        <option> <?php echo 'Comidas'; ?></option>
                                        <option> <?php echo 'Transporte'; ?></option>
					<option> <?php echo 'Lazer'; ?></option>
                                        <option> <?php echo 'Viagens'; ?></option>
                                        <option> <?php echo 'Outros'; ?></option>
                                    
                                    <?php } elseif($tipo == 'Outros') { ?>
                                    
                                        <option> <?php echo 'Estudos'; ?></option>
                                        <option> <?php echo 'Comidas'; ?></option>
                                        <option> <?php echo 'Transporte'; ?></option>
					<option> <?php echo 'Lazer'; ?></option>
                                        <option> <?php echo 'Viagens'; ?></option>
                                        <option> <?php echo 'Recarga Celular'; ?></option>
                                        
                                    <?php } else{ ?>
                                    
                                        <option> <?php echo 'Estudos'; ?></option>
                                        <option> <?php echo 'Comidas'; ?></option>
                                        <option> <?php echo 'Transporte'; ?></option>
                                        <option> <?php echo 'Lazer'; ?></option>
                                        <option> <?php echo 'Recarga Celular'; ?></option>
                                        <option> <?php echo 'Outros'; ?></option>
                                        
                                    <?php }?>
                                </select></td>
                        </tr>
						<tr>
                            <td><label class="control-label">Forma de Pagamento:</label></td>
                            <td><select class="form-control"  name="forma_pagamento" required >
                                    <option><?php echo $forma_pagamento; ?></option>
                                        
                                    <?php if($forma_pagamento == 'Dinheiro') { ?>
                                    
                                        <option> <?php echo 'Cartão de Crédito'; ?></option>
                                        <option> <?php echo 'Cartão de Débito'; ?></option>
										<option> <?php echo 'Cheque'; ?></option>
                                        <option> <?php echo 'Outros'; ?></option>
                                        
                                    <?php } elseif($forma_pagamento == 'Cartão de Crédito') { ?>
                                    
                                        <option> <?php echo 'Dinheiro'; ?></option>
                                        <option> <?php echo 'Cartão de Débito'; ?></option>
										<option> <?php echo 'Cheque'; ?></option>
                                        <option> <?php echo 'Outros'; ?></option>
                                        
                                    <?php } elseif($forma_pagamento == 'Cartão de Débito') { ?>
                                    
                                        <option> <?php echo 'Dinheiro'; ?></option>
                                        <option> <?php echo 'Cartão de Crédito'; ?></option>
										<option> <?php echo 'Cheque'; ?></option>
                                        <option> <?php echo 'Outros'; ?></option>
										
									<?php } elseif($forma_pagamento == 'Cheque') { ?>
                                    
                                        <option> <?php echo 'Dinheiro'; ?></option>
                                        <option> <?php echo 'Cartão de Crédito'; ?></option>
										<option> <?php echo 'Cartão de Débito'; ?></option>
                                        <option> <?php echo 'Outros'; ?></option>
                                        
                                    <?php } else{ ?>
                                    
                                        <option> <?php echo 'Dinheiro'; ?></option>
                                        <option> <?php echo 'Cartão de Crédito'; ?></option>
										<option> <?php echo 'Cartão de Débito'; ?></option>
                                        <option> <?php echo 'Cheque'; ?></option>
                                        
                                    <?php }?>
                                </select></td>
                        </tr>
                        <tr>
                            <td><label class="control-label">Data:</label></td>
                            <td>
                                <div style="width:200px; margin-left: 6px; margin-top:8px;" class="form-group">
                                    <div class='input-group date' id='datetimepicker4'>
                                        <input name="data" type='text' value="<?php echo $data; ?>" class="form-control" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                <script type="text/javascript">
                                    $(function () {
                                        $('#datetimepicker4').datetimepicker({
                                            format: 'YYYY-MM-DD'
                                        });
                                    });
                                </script>
                            </td>                                     
                        </tr>
                        <tr>
                            <td colspan="2">
                                <button type="submit" name="btn_save_updates" class="btn btn-default">
                                    <span class="glyphicon glyphicon-save"></span> Editar
                                </button>

                                <a class="btn btn-default" href="contas_pagar.php"> <span class="glyphicon glyphicon-backward"></span> Cancelar </a>

                            </td>
                        </tr>

                    </table>

                </form>

            </div>
        </div>
    </body>
</html>