<?php
require_once 'dbconnect.php';

if (isset($_GET['edit_id']) && !empty($_GET['edit_id'])) {

    $usuario = $_GET['edit_id'];
} else {
    header("Location: contas_pagar.php");
}

$res = mysql_query("SELECT * FROM usuarios WHERE id_usuarios=" . $usuario);
$userRow = mysql_fetch_array($res);

if (isset($_POST['btn_insert'])) {


    $apelido = $_POST['apelido'];

    $source = array('.', ',');
    $replace = array('', '.');
    $valor = str_replace($source, $replace, $_POST['valor']);

    $tipo = $_POST['tipo'];
    $forma_pagamento = $_POST['forma_pagamento'];
    $data = $_POST['data'];

    $stmt = mysql_query("INSERT INTO contas_pagar(id_usuarios, apelido, valor, tipo, forma_pagamento, data) VALUES ('" . $usuario . "','" . $apelido . "','" . $valor . "','" . $tipo . "','" . $forma_pagamento . "','" . $data . "')");
    ?>
    <script>
        alert('Conta a Pagar inserida com sucesso ...');
        window.location.href = 'contas_pagar.php';
    </script>

    <?php
}
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Criar Conta a Pagar</title>
        <link rel="stylesheet" href="assets/css/bootstrap.min.css" type="text/css"  />
        <link rel="stylesheet" href="style.css" type="text/css" />
        <script>
            var myChart = new Chart({...})

        </script>
        <link rel="stylesheet" type="text/css" media="screen" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" />
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
        <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
        <script src="https://igorescobar.github.io/jQuery-Mask-Plugin/js/jquery.mask.min.js"></script>           
        <script language="javascript">
            function MascaraMoeda(objTextBox, SeparadorMilesimo, SeparadorDecimal, e) {
                var sep = 0;
                var key = '';
                var i = j = 0;
                var len = len2 = 0;
                var strCheck = '0123456789';
                var aux = aux2 = '';
                var whichCode = (window.Event) ? e.which : e.keyCode;
                if (whichCode == 13)
                    return true;
                key = String.fromCharCode(whichCode); // Valor para o código da Chave
                if (strCheck.indexOf(key) == -1)
                    return false; // Chave inválida
                len = objTextBox.value.length;
                for (i = 0; i < len; i++)
                    if ((objTextBox.value.charAt(i) != '0') && (objTextBox.value.charAt(i) != SeparadorDecimal))
                        break;
                aux = '';
                for (; i < len; i++)
                    if (strCheck.indexOf(objTextBox.value.charAt(i)) != -1)
                        aux += objTextBox.value.charAt(i);
                aux += key;
                len = aux.length;
                if (len == 0)
                    objTextBox.value = '';
                if (len == 1)
                    objTextBox.value = '0' + SeparadorDecimal + '0' + aux;
                if (len == 2)
                    objTextBox.value = '0' + SeparadorDecimal + aux;
                if (len > 2) {
                    aux2 = '';
                    for (j = 0, i = len - 3; i >= 0; i--) {
                        if (j == 3) {
                            aux2 += SeparadorMilesimo;
                            j = 0;
                        }
                        aux2 += aux.charAt(i);
                        j++;
                    }
                    objTextBox.value = '';
                    len2 = aux2.length;
                    for (i = len2 - 1; i >= 0; i--)
                        objTextBox.value += aux2.charAt(i);
                    objTextBox.value += SeparadorDecimal + aux.substr(len - 2, len);
                }
                return false;
            }
        </script>
    </head>
    <body>
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="home.php">FinanceOne</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="home.php">Dashboard</a></li>
                        <li class="active"><a href="contas_pagar.php">Contas a Pagar</a></li>
                        <li><a href="contas_receber.php">Contas a Receber</a></li>
                        <li><a href="investimentos.php">Investimentos</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <span class="glyphicon glyphicon-user"></span>&nbsp;Ol&aacute; <?php echo $userRow['usuario']; ?>&nbsp;<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="logout.php?logout"><span class="glyphicon glyphicon-log-out"></span>&nbsp;Sair</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div id="wrapper" align="center">
            <div class="container" style="width: 30%">
                <div class="page-header">
                    <h1 class="h2">Inserir Conta a Pagar <a class="btn btn-default" href="contas_pagar.php"> Voltar </a></h1>
                </div>
                <form method="post" enctype="multipart/form-data" class="form-horizontal">
                    <?php
                    if (isset($errMSG)) {
                        ?>
                        <div class="alert alert-danger">
                            <span class="glyphicon glyphicon-info-sign"></span> &nbsp; <?php echo $errMSG; ?>
                        </div>
    <?php
}
?>
                    <table class="table table-bordered table-responsive">
                        <tr>
                            <td><label class="control-label">Apelido da Conta:</label></td>
                            <td><input class="form-control" type="text" name="apelido" placeholder="Exemplo: Faculdade" required/></td>
                        </tr>
                        <tr>
                            <td><label class="control-label">Valor:<span style="padding-left: 120px;">R$</span></label></td>
                            <td><input class="form-control" placeholder="" type="text" name="valor"  onKeyPress="return(MascaraMoeda(this, '.', ',', event))"/></td>
                        </tr>

                        <tr>
                            <td><label class="control-label">Tipo de Conta:</label></td>
                            <td>
                                <select class="form-control"  name="tipo">
                                    <option>Estudos</option>
                                    <option>Comidas</option>
                                    <option>Lazer</option>
                                    <option>Transporte</option>
                                    <option>Recarga Celular</option>
                                    <option>Viagens</option>
                                    <option>Outros</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td><label class="control-label">Forma de Pagamento:</label></td>
                            <td>
                                <select class="form-control"  name="forma_pagamento">
                                    <option>Dinheiro</option>
                                    <option>Cartão de Crédito</option>
                                    <option>Cartão de Débito</option>
                                    <option>Cheque</option>
                                    <option>Outros</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td><label class="control-label">Data:</label></td>
                            <td>
                                <div style="width:200px; margin-left: 6px; margin-top:8px;" class="form-group">
                                    <div class='input-group date' id='datetimepicker4'>
                                        <input name="data" type='text' class="form-control" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                <script type="text/javascript">
                                    $(function () {
                                        $('#datetimepicker4').datetimepicker({
                                            format: 'YYYY-MM-DD'
                                        });
                                    });
                                </script>
                            </td>                                     
                        </tr>
                        </div>
                        </div>
                        <tr>
                            <td colspan="2">
                                <button type="submit" name="btn_insert" class="btn btn-default">
                                    <span class="glyphicon glyphicon-save"></span> Inserir
                                </button>
                                <a class="btn btn-default" href="contas_pagar.php"> <span class="glyphicon glyphicon-backward"></span> Cancelar </a>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </body>
</html>