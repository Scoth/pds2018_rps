<?php
ob_start();
session_start();
require_once 'dbconnect.php';

if (!isset($_SESSION['user'])) {
    header("Location: index.php");
    exit;
}

$res = mysql_query("SELECT * FROM usuarios WHERE id_usuarios=" . $_SESSION['user']);
$userRow = mysql_fetch_array($res);

print_r($userRow['id_usuarios']);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>FinanceOne - <?php echo $userRow['usuario']; ?></title>
        <link rel="stylesheet" href="assets/css/bootstrap.min.css" type="text/css"  />
        <link rel="stylesheet" href="style.css" type="text/css" />
        <script src="assets/jquery-1.11.3-jquery.min.js"></script>  
        <script src="assets/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    </head>
    <body>

        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="home.php">FinanceOne</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="home.php">Dashboard</a></li>
                        <li><a href="contas_pagar.php">Contas a Pagar</a></li>
                        <li><a href="contas_receber.php">Contas a Receber</a></li>
                        <li><a href="investimentos.php">Investimentos</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <span class="glyphicon glyphicon-user"></span>&nbsp;Ol&aacute; <?php echo $userRow['usuario']; ?>&nbsp;<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="logout.php?logout"><span class="glyphicon glyphicon-log-out"></span>&nbsp;Sair</a></li>
                            </ul>
                        </li>
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </nav> 

        <div id="wrapper">

            <div class="container">

                <div class="page-header">
                    <h3>FinanceOne - Dashboard</h3>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <h1>Acompanhe os gr&aacute;ficos de suas contas</h1>
                    </div>
                </div>
                <div align="center">
                    <div id="piechart" align="center" style="width: 1024px; height: 569px; border: 1px solid #ccc"></div>
                    <br>
                    <div id="barchart_values" align="center" style="width: 1024px; height: 569px; border: 1px solid #ccc"></div>
                    <br>
                    <div id="curve_chart" align="center" style="width: 1024px; height: 569px; border: 1px solid #ccc"></div>

                </div>
                <script type="text/javascript">
                    google.charts.load('current', {'packages': ['corechart']});
                    google.charts.load('current', {'packages': ['bar']});
                    google.charts.setOnLoadCallback(drawChart);

                    function drawChart() {
                        var id_usuarios = <?= $userRow['id_usuarios'] ?>;
                        console.log(id_usuarios);
                        $.ajax({
                            method: "GET",
                            url: "processa.php",
                            success: function (jsondata) {
                                obj = $.parseJSON(jsondata);
                                console.log(jsondata);

                                var data = google.visualization.arrayToDataTable([
                                    ['Tipo de Conta', 'Gasto'],
                                    ['Estudos', obj.cont_estudos],
                                    ['Alimentação', obj.cont_comidas],
                                    ['Lazer', obj.cont_lazer],
                                    ['Transporte', obj.cont_transporte],
                                    ['Recarga Celular', obj.cont_recarga],
                                    ['Viagens', obj.cont_viagem],
                                    ['Outros', obj.cont_outros]
                                ]);
                                
                                var data2 = new google.visualization.arrayToDataTable([
                                    ['Ativo/Passivo', 'Valor', { role: 'style' } ],
                                    ['Recebido', parseFloat(obj.p1), 'green'],
                                    ['Gasto', parseFloat(obj.p2), 'red'],
                                    ['Total', parseFloat(obj.p3), 'blue']
                                ]);
                                var data3 = google.visualization.arrayToDataTable([
                                    ['Meses', 'Rentabilidade'],
                                    ['Mes 1',  parseFloat(obj.i1)],
                                    ['Mes 2',  parseFloat(obj.i2)],
                                    ['Mes 3',  parseFloat(obj.i3)],
                                    ['Mes 4',  parseFloat(obj.i4)],
                                    ['Mes 5',  parseFloat(obj.i5)],
                                    ['Mes 6',  parseFloat(obj.i6)],
                                    ['Mes 7',  parseFloat(obj.i7)],
                                    ['Mes 8',  parseFloat(obj.i8)],
                                    ['Mes 9',  parseFloat(obj.i9)],
                                    ['Mes 10',  parseFloat(obj.i10)],
                                    ['Mes 11',  parseFloat(obj.i11)],
                                    ['Mes 12',  parseFloat(obj.i12)]
                                  ]);
                                  
                                var view = new google.visualization.DataView(data2);
                                view.setColumns([0, 1,
                                                 { calc: "stringify",
                                                   sourceColumn: 1,
                                                   type: "string",
                                                   role: "annotation" },
                                                 2]);
                                             
                                var options = {
                                    title: 'Valor gasto por tipo'
                                };
                                var options2 = {
                                    title: 'Contas a Pagar e a Receber',
                                    width: 800,
                                    chart: {
                                        title: 'Ativo/Passivo',
                                        subtitle: ''
                                    },
                                    bars: 'horizontal', // Required for Material Bar Charts.
                                    series: {
                                        0: {axis: 'distance'}, // Bind series 0 to an axis named 'distance'.
                                        1: {axis: 'brightness'} // Bind series 1 to an axis named 'brightness'.
                                    },
                                    axes: {
                                        x: {
                                            distance: {label: 'parsecs'}, // Bottom x-axis.
                                            brightness: {side: 'top', label: 'apparent magnitude'} // Top x-axis.
                                        }
                                    }
                                };
                                var options3 = {
                                    title: 'Projeção de Investimento',
                                    curveType: 'function',
                                    legend: { position: 'bottom' }
                                };
                                var chart = new google.visualization.PieChart(document.getElementById('piechart'));
                                chart.draw(data, options);
                                
                                var chart = new google.visualization.BarChart(document.getElementById("barchart_values"));
                                chart.draw(view, options2);
                            
                                var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));
                                chart.draw(data3, options3);

                            }
                        });
                    }

                    $(document).ready(function () {
                        drawChart();

                    });

                    $('#filtrar').click(function () {
                        console.log('EEEETE');
                        drawChart();
                    });
                </script>
            </div>
        </div>
    </body>
</html>
<?php ob_end_flush(); ?>