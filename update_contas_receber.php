<?php
require_once 'dbconnect.php';

if (isset($_GET['edit_id']) && !empty($_GET['edit_id'])) {

    $id = $_GET['edit_id'];
    $select_edit = mysql_query("SELECT * FROM contas_receber WHERE id_contas_receber =" . $id);

    $row = mysql_fetch_array($select_edit);

    $valor = $row['valor']; 
    $fonte_renda = $row['fonte_renda'];
    $data = $row['data'];

    //echo '<pre style="padding-top:150px;">';
    //    var_dump($row['apelido']);
    //echo '</pre>';
    
} else {
    header("Location: contas_receber.php");
}

$res = mysql_query("SELECT * FROM usuarios WHERE id_usuarios=" . $row['id_usuarios']);
$userRow = mysql_fetch_array($res);

if (isset($_POST['btn_save_updates'])) {

    $valor2 = $_POST['valor']; 
    $fonte_renda2 = $_POST['fonte_renda'];
    $data2 = $_POST['data'];

    $stmt = mysql_query("UPDATE contas_receber
                                    SET valor='" . $valor2 . "',
                                        fonte_renda='" . $fonte_renda2 . "',
                                        data='" . $data2 . "'
                              WHERE id_contas_receber=" . $id);
    ?>
    <script>
        alert('Atualizado com sucesso ...');
        window.location.href = 'contas_receber.php';
    </script>
    <?php
}
?>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Alterar Contas a Receber</title>
        <link rel="stylesheet" href="assets/css/bootstrap.min.css" type="text/css"  />
        <link rel="stylesheet" href="style.css" type="text/css" />
        <script>
    var myChart = new Chart({...})
        </script>
        <link rel="stylesheet" type="text/css" media="screen" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" />
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
        <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
        <script src="https://igorescobar.github.io/jQuery-Mask-Plugin/js/jquery.mask.min.js"></script>  
    </head>
    <body>

        <nav class="navbar navbar-default navbar-fixed-top">
         <div class="container">
            <div class="navbar-header">
               <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
               <span class="sr-only">Toggle navigation</span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               </button>
               <a class="navbar-brand" href="home.php">FinanceOne</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
               <ul class="nav navbar-nav">
                  <li><a href="home.php">Dashboard</a></li>
                  <li><a href="contas_receber.php">Contas a Pagar</a></li>
                  <li class="active"><a href="contas_receber.php">Contas a Receber</a></li>
                  <li><a href="investimentos.php">Investimentos</a></li>
               </ul>
               <ul class="nav navbar-nav navbar-right">
                  <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                     <span class="glyphicon glyphicon-user"></span>&nbsp;Ol&aacute; <?php echo $userRow['usuario']; ?>&nbsp;<span class="caret"></span></a>
                     <ul class="dropdown-menu">
                        <li><a href="logout.php?logout"><span class="glyphicon glyphicon-log-out"></span>&nbsp;Sair</a></li>
                     </ul>
                  </li>
               </ul>
            </div>
         </div>
		</nav>

        <div id="wrapper" align="center">
            <div class="container" style="width: 30%">


                <div class="page-header">
                    <h1 class="h2">Alterar Contas a Receber <a class="btn btn-default" href="contas_receber.php"> Voltar </a></h1>
                </div>

                <form method="post" enctype="multipart/form-data" class="form-horizontal">


                    <?php
                    if (isset($errMSG)) {
                        ?>
                        <div class="alert alert-danger">
                            <span class="glyphicon glyphicon-info-sign"></span> &nbsp; <?php echo $errMSG; ?>
                        </div>
                        <?php
                    }
                    ?>


                    <table class="table table-bordered table-responsive">
                        <tr>
                            <td><label class="control-label">Valor:</label></td>
                            <td><input class="form-control" type="text" name="valor" value="<?php echo $valor; ?>" required /></td>
                        </tr>

                        <tr>
                            <td><label class="control-label">Fonte de Renda:</label></td>
                            <td><select class="form-control"  name="fonte_renda" required >
                                    <option><?php echo $fonte_renda; ?></option>
                                        
                                    <?php if($fonte_renda == 'Trabalho') { ?>
                                    
                                        <option> <?php echo 'Aposentadoria'; ?></option>
                                        <option> <?php echo 'Rendimento por Aplicação'; ?></option>
                                        <option> <?php echo 'Trabalho Extras'; ?></option>
                                        <option> <?php echo 'Outros'; ?></option>
                                        
                                    <?php } elseif($fonte_renda == 'Aposentadoria') { ?>
                                    
                                        <option> <?php echo 'Trabalho'; ?></option>
                                        <option> <?php echo 'Rendimento por Aplicação'; ?></option>
                                        <option> <?php echo 'Trabalho Extras'; ?></option>
                                        <option> <?php echo 'Outros'; ?></option>
                                        
                                    <?php } elseif($fonte_renda == 'Rendimento por Aplicação') { ?>
                                    
                                        <option> <?php echo 'Trabalho'; ?></option>
                                        <option> <?php echo 'Aposentadoria'; ?></option>
                                        <option> <?php echo 'Trabalho Extras'; ?></option>
                                        <option> <?php echo 'Outros'; ?></option>
										
                                    <?php } elseif($fonte_renda == 'Trabalho Extras') { ?>
                                    
                                        <option> <?php echo 'Trabalho'; ?></option>
                                        <option> <?php echo 'Rendimento por Aplicação'; ?></option>
                                        <option> <?php echo 'Aposentadoria'; ?></option>
                                        <option> <?php echo 'Outros'; ?></option>
                                                                            
                                    <?php } else{ ?>
                                    
                                        <option> <?php echo 'Trabalho'; ?></option>
                                        <option> <?php echo 'Aposentadoria'; ?></option>
                                        <option> <?php echo 'Trabalho Extras'; ?></option>
                                        <option> <?php echo 'Rendimento por Aplicação'; ?></option>
                                        
                                    <?php }?>
                                </select></td>
                        </tr>
						
                        <tr>
                            <td><label class="control-label">Data:</label></td>
                            <td>
                                <div style="width:200px; margin-left: 6px; margin-top:8px;" class="form-group">
                                    <div class='input-group date' id='datetimepicker4'>
                                        <input name="data" type='text' value="<?php echo $data; ?>" class="form-control" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                <script type="text/javascript">
                                    $(function () {
                                        $('#datetimepicker4').datetimepicker({
                                            format: 'YYYY-MM-DD'
                                        });
                                    });
                                </script>
                            </td>  

                        <tr>
                            <td colspan="2">
                                <button type="submit" name="btn_save_updates" class="btn btn-default">
                                    <span class="glyphicon glyphicon-save"></span> Editar
                                </button>

                                <a class="btn btn-default" href="contas_receber.php"> <span class="glyphicon glyphicon-backward"></span> Cancelar </a>

                            </td>
                        </tr>

                    </table>

                </form>

            </div>
        </div>
    </body>
</html>