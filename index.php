<?php
ob_start();
session_start();
require_once 'dbconnect.php';

if (isset($_SESSION['user']) != "") {
    header("Location: home.php");
    exit;
}

$error = false;

if (isset($_POST['btn-login'])) {

    $email = trim($_POST['email']);
    $email = strip_tags($email);
    $email = htmlspecialchars($email);

    $pass = trim($_POST['pass']);
    $pass = strip_tags($pass);
    $pass = htmlspecialchars($pass);

    if (empty($email)) {
        $error = true;
        $emailError = "Preencha com seu email.";
    } else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $error = true;
        $emailError = "Por favor entre com um email válido.";
    }

    if (empty($pass)) {
        $error = true;
        $passError = "Preencha com a sua senha.";
    }

    if (!$error) {

        $res = mysql_query("SELECT id_usuarios, usuario, senha FROM usuarios WHERE email='$email'");
        $row = mysql_fetch_array($res);
        $count = mysql_num_rows($res); // if uname/pass correct it returns must be 1 row

        if ($count == 1 && $row['senha'] == $pass) {
            $_SESSION['user'] = $row['id_usuarios'];
            header("Location: home.php");
        } else {
            $errMSG = "Credenciais Incorretas. Tente novamente...";
        }
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>FinanceOne - Sistema Financeiro</title>
        <link rel="stylesheet" href="assets/css/bootstrap.min.css" type="text/css"  />
        <link rel="stylesheet" href="style.css" type="text/css" />
        <style>
            .back-index{
                background-image: url(assets/img/index.png);
                background-repeat: no-repeat;
                background-attachment: fixed;
                background-position: center;
            }
        </style>
    </head>
    <body class="back-index">

        <div class="container">

            <div id="login-form">
                <form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" autocomplete="off">

                    <div class="col-md-12">

                        <div class="form-group">
                            <h2 align="center" style="color: white">Seja Bem Vindo ao Portal Financeiro - FinanceOne.</h2>
                        </div>

                        <div class="form-group">
                            <hr />
                        </div>

                        <?php
                        if (isset($errMSG)) {
                            ?>
                            <div class="form-group">
                                <div class="alert alert-danger">
                                    <span class="glyphicon glyphicon-info-sign"></span> <?php echo $errMSG; ?>
                                </div>
                            </div>
                            <?php
                        }
                        ?>

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span>
                                <input type="email" name="email" class="form-control" placeholder="Seu Email" value="<?php echo $email; ?>" maxlength="40" />
                            </div>
                            <span class="text-danger"><?php echo $emailError; ?></span>
                        </div>

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                                <input type="password" name="pass" class="form-control" placeholder="Sua Senha" maxlength="15" />
                            </div>
                            <span class="text-danger"><?php echo $passError; ?></span>
                        </div>

                        <div class="form-group">
                            <hr />
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-block btn-primary" name="btn-login">Entrar.</button>
                        </div>

                        <div class="form-group">
                            <hr />
                        </div>

                        <div class="form-group">
                            <a href="register.php" style="color: white">Registre-se aqui...</a>
                        </div>

                    </div>

                </form>
            </div>	

        </div>

    </body>
</html>
<?php ob_end_flush(); ?>