<?php

require_once 'dbconnect.php';
session_start();
$id_usuarios = $_SESSION['user'];

$contagem = mysql_query("SELECT * FROM contas_pagar WHERE id_usuarios = " . $id_usuarios);

$cont_estudos = 0;
$cont_comidas = 0;
$cont_lazer = 0;
$cont_transporte = 0;
$cont_recarga = 0;
$cont_viagem = 0;
$cont_outros = 0;

while ($row = mysql_fetch_array($contagem)) {

    //echo $row['tipo'];
    if ($row['tipo'] == 'Estudos') {
        $cont_estudos+= $row['valor'];
        
    } elseif ($row['tipo'] == 'Comidas') {
        $cont_comidas+= $row['valor'];
        
    } elseif ($row['tipo'] == 'Lazer') {
        $cont_lazer+= $row['valor'];
        
    } elseif ($row['tipo'] == 'Transporte') {
        $cont_transporte+= $row['valor'];
        
    } elseif ($row['tipo'] == 'Recarga Celular') {
        $cont_recarga+= $row['valor'];
        
    } elseif ($row['tipo'] == 'Viagens') {
        $cont_viagem+= $row['valor'];
        
    } elseif ($row['tipo'] == 'Outros'){
        $cont_outros+= $row['valor'];
    }

}

$passivo = mysql_query("SELECT SUM(valor) FROM contas_pagar WHERE id_usuarios = " . $id_usuarios);
$ativo = mysql_query("SELECT SUM(valor) FROM contas_receber WHERE id_usuarios = " . $id_usuarios);

$receber = mysql_fetch_array($ativo);
$pagar = mysql_fetch_array($passivo);

$positivo = $receber['SUM(valor)'];
$positivo_duas_casas = number_format($positivo, 2, '.', '');

$negativo = $pagar['SUM(valor)'];
$negativo2 = $negativo * -1;
$negativo_duas_casas = number_format($negativo2, 2, '.', '');

$total = ($positivo - $negativo);
$total_duas_casas = number_format($total, 2, '.', '');

$investimento = mysql_query("SELECT * FROM investimentos WHERE id_usuarios = " . $id_usuarios);
$valor = mysql_fetch_array($investimento);

$inv = $valor['valor'];
$porc = $valor['juros'];

for ($i = 0; $i < 12; $i++) {
    
    $timo[$i] = $inv + (($inv/100)* $porc);
    $timo2[$i] = number_format($timo[$i], 2, '.', '');
    
    if ($i > 0){
        $timo[$i] = $timo[$i-1] + (($timo[$i-1]/100)* $porc);
        $timo2[$i] = number_format($timo[$i], 2, '.', '');

    }

}

$array_dados = Array(
    'cont_estudos' => $cont_estudos, 
    'cont_comidas' => $cont_comidas, 
    'cont_lazer' => $cont_lazer, 
    'cont_transporte' => $cont_transporte, 
    'cont_recarga' => $cont_recarga, 
    'cont_viagem' => $cont_viagem, 
    'cont_outros' => $cont_outros,
    'p1' => $positivo_duas_casas,
    'p2' => $negativo_duas_casas,
    'p3' => $total_duas_casas,
    'i1' => $timo2[0],
    'i2' => $timo2[1],
    'i3' => $timo2[2],
    'i4' => $timo2[3],
    'i5' => $timo2[4],
    'i6' => $timo2[5],
    'i7' => $timo2[6],
    'i8' => $timo2[7],
    'i9' => $timo2[8],
    'i10' => $timo2[9],
    'i11' => $timo2[10],
    'i12' => $timo2[11]
        );

    echo json_encode($array_dados);

