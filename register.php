<?php
ob_start();
session_start();
if (isset($_SESSION['user']) != "") {
    header("Location: home.php");
}
include_once 'dbconnect.php';

$error = false;

if (isset($_POST['btn-signup'])) {

    $name = trim($_POST['name']);
    $name = strip_tags($name);
    $name = htmlspecialchars($name);

    $email = trim($_POST['email']);
    $email = strip_tags($email);
    $email = htmlspecialchars($email);

    $pass = trim($_POST['pass']);
    $pass = strip_tags($pass);
    $pass = htmlspecialchars($pass);

    if (empty($name)) {
        $error = true;
        $nameError = "Por favor entre com o seu Nome.";
    } else if (strlen($name) < 5) {
        $error = true;
        $nameError = "Deve conter  ao menos 5 caracteres.";
    } else if (!preg_match("/^[a-zA-Z ]+$/", $name)) {
        $error = true;
        $nameError = "Deve conter letras alfabéticas e espaçamento.";
    }

    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $error = true;
        $emailError = "Please enter valid email address.";
    } else {
        $query = "SELECT email FROM usuarios WHERE email='$email'";
        $result = mysql_query($query);
        $count = mysql_num_rows($result);
        if ($count != 0) {
            $error = true;
            $emailError = "O email já existe.";
        }
    }
    if (empty($pass)) {
        $error = true;
        $passError = "Por favor entre com uma senha válida.";
    } else if (strlen($pass) < 4) {
        $error = true;
        $passError = "Senha deve ter ao mínimo 4 caracters.";
    }

    if (!$error) {

        $query = "INSERT INTO usuarios(usuario,email,senha) VALUES('$name','$email','$pass')";
        $res = mysql_query($query);

        if ($res) {
            $errTyp = "success";
            $errMSG = "Usuário registrado com sucesso!";
            unset($name);
            unset($email);
            unset($pass);
        } else {
            $errTyp = "danger";
            $errMSG = "Alguma coisa deu errado, tente mais tarde...";
        }
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Coding Cage - Login & Registration System</title>
        <link rel="stylesheet" href="assets/css/bootstrap.min.css" type="text/css"  />
        <link rel="stylesheet" href="style.css" type="text/css" />
    </head>
    <body>

        <div class="container">

            <div id="login-form">
                <form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" autocomplete="off">

                    <div class="col-md-12">

                        <div class="form-group">
                            <h2 class="">Registre-se</h2>
                        </div>

                        <div class="form-group">
                            <hr />
                        </div>

<?php
if (isset($errMSG)) {
    ?>
                            <div class="form-group">
                                <div class="alert alert-<?php echo ($errTyp == "success") ? "success" : $errTyp; ?>">
                                    <span class="glyphicon glyphicon-info-sign"></span> <?php echo $errMSG; ?>
                                </div>
                            </div>
    <?php
}
?>

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
                                <input type="text" name="name" class="form-control" placeholder="Nome" maxlength="50" value="<?php echo $name ?>" />
                            </div>
                            <span class="text-danger"><?php echo $nameError; ?></span>
                        </div>

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span>
                                <input type="email" name="email" class="form-control" placeholder="Email" maxlength="40" value="<?php echo $email ?>" />
                            </div>
                            <span class="text-danger"><?php echo $emailError; ?></span>
                        </div>

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                                <input type="password" name="pass" class="form-control" placeholder="Senha" maxlength="15" />
                            </div>
                            <span class="text-danger"><?php echo $passError; ?></span>
                        </div>

                        <div class="form-group">
                            <hr />
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-block btn-primary" name="btn-signup">Registrar</button>
                        </div>

                        <div class="form-group">
                            <hr />
                        </div>

                        <div class="form-group">
                            <a href="index.php">Já possui conta? Acessar aqui...</a>
                        </div>

                    </div>

                </form>
            </div>	

        </div>

    </body>
</html>
<?php ob_end_flush(); ?>