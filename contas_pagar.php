<?php
ob_start();
session_start();
require_once 'dbconnect.php';

if (!isset($_SESSION['user'])) {
    header("Location: index.php");
    exit;
}
$res = mysql_query("SELECT * FROM usuarios WHERE id_usuarios=" . $_SESSION['user']);
$userRow = mysql_fetch_array($res);

if (isset($_GET['delete_id'])) {
    $delete = mysql_query("DELETE FROM contas_pagar WHERE id_contas_pagar =" . $_GET['delete_id']) or die(mysql_error());

    header("Location: contas_pagar.php");
}
?>
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Contas a Pagar - <?php echo $userRow['usuario']; ?></title>
        <link rel="stylesheet" href="assets/css/bootstrap.min.css" type="text/css"  />
        <link rel="stylesheet" href="style.css" type="text/css" />
        <script>
            var myChart = new Chart({...}
            )
        </script>
    </head>
    <body>

        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="home.php">FinanceOne</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="home.php">Dashboard</a></li>
                        <li class="active"><a href="contas_pagar.php">Contas a Pagar</a></li>
                        <li><a href="contas_receber.php">Contas a Receber</a></li>
                        <li><a href="investimentos.php">Investimentos</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <span class="glyphicon glyphicon-user"></span>&nbsp;Ol&aacute; <?php echo $userRow['usuario']; ?>&nbsp;<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="logout.php?logout"><span class="glyphicon glyphicon-log-out"></span>&nbsp;Sair</a></li>
                            </ul>
                        </li>
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </nav> 

        <div id="wrapper">

            <div class="container">

                <div class="page-header">
                    <h3>FinanceOne</h3>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <h1 align="center">Visualize suas Contas a Pagar.</h1><br><br>
                    </div>
                </div>

                <?php
                $query = mysql_query("SELECT  a.id_contas_pagar ,a.apelido, a.valor, a.tipo, a.forma_pagamento, a.data FROM contas_pagar as a
                        JOIN usuarios as b ON (a.id_usuarios = b.id_usuarios)
                        where a.id_usuarios = " . $_SESSION['user'] . " order by a.id_contas_pagar") or die(mysql_error());
                ?>


                <div class="container" align="center">
                    <div class="panel-heading">
                        <h2>Contas a Pagar</h2>
                        <div class="btn-group pull-right">
                            <a class="btn btn-success" href="create_contas_pagar.php?edit_id=<?php echo $_SESSION['user']; ?>"><span class="glyphicon glyphicon-plus"></span> Novo</a> 
                        </div>
                    </div>
                    <table class="table table-responsive">
                        <thead>
                            <tr>
                                <th>Apelido da Conta</th>
                                <th>Valor</th>
                                <th>Tipo de Conta</th>
                                <th>Forma de Pagamento</th>
                                <th>Data</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <?php while ($row = mysql_fetch_array($query, MYSQL_ASSOC)) { ?>
                            <tbody>
                                <tr>
                                    <td><?php echo $row['apelido']; ?></td>
                                    <td>R$ <?php echo $row['valor']; ?></td>
                                    <td><?php echo $row['tipo']; ?></td>
                                    <td><?php echo $row['forma_pagamento']; ?></td>
                                    <td><?php
                            $originalDate = $row['data'];
                            $newDate = date("d-m-Y", strtotime($originalDate));
                            echo $newDate;
                            ?></td>
                                    <td>
                                        <a class="btn btn-info" href="update_contas_pagar.php?edit_id=<?php echo $row['id_contas_pagar']; ?>"><span class="glyphicon glyphicon-edit"></span> Editar</a> 
                                        <a data-toggle="topper" data-target="#view-topper" class="btn btn-danger" href="?delete_id=<?php echo $row['id_contas_pagar']; ?>" title="Deletar Aluno" onclick="return confirm('Você tem certeza que deseja deletar a conta a pagar?')"><span class="glyphicon glyphicon-remove-circle"></span> Deletar</a>
                                    </td>
                                </tr> 
                            </tbody>
                        <?php } ?>
                    </table>
                </div>
            </div>
        </div>

        <script src="assets/jquery-1.11.3-jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="bootbox.min.js"></script>
        <script>
            $('.deleteUser').click(function (e) {

            e.preventDefault();
                    var pid = $(this).attr('data-value');
                    var parent = $(this).parent("td").parent("tr");
                    bootbox.dialog({
                    message: "Você tem certeza que deseja excluir o Aluno ?",
                            title: "<i class='glyphicon glyphicon-trash'></i> Excluir Aluno",
                            buttons: {
                            success: {
                            label: "No",
                                    className: "btn-success",
                                    callback: function () {
                                    $('.bootbox').modal('hide');
                                    }
                            },
                                    danger: {
                                    label: "Excluir",
                                            className: "btn-danger",
                                            callback: function () {

                                            $.post('delete.php', {'delete': pid})
                                                    .done(function (response) {
                                                    bootbox.alert(response);
                                                            parent.fadeOut('slow');
                                                    })
                                                    .fail(function () {
                                                    bootbox.alert('Something Went Wrog ....');
                                                    })

                                            }
                                    }
                            }
                    });
            });
            }
            );
        </script>
    </body>
</html>
<?php ob_end_flush(); ?>