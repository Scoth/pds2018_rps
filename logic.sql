-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 19-Nov-2018 às 00:48
-- Versão do servidor: 10.1.36-MariaDB
-- versão do PHP: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `logic`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `contas_pagar`
--

CREATE TABLE `contas_pagar` (
  `id_contas_pagar` int(11) NOT NULL,
  `id_usuarios` int(11) NOT NULL,
  `apelido` varchar(120) NOT NULL,
  `valor` float NOT NULL,
  `tipo` varchar(255) DEFAULT NULL,
  `forma_pagamento` varchar(255) DEFAULT NULL,
  `data` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `contas_pagar`
--

INSERT INTO `contas_pagar` (`id_contas_pagar`, `id_usuarios`, `apelido`, `valor`, `tipo`, `forma_pagamento`, `data`) VALUES
(26, 8, 'Aluguel', 950, 'Outros', 'Cheque', '2018-11-08'),
(27, 8, 'Caribe', 7500, 'Viagens', 'Cheque', '2018-11-22'),
(28, 8, 'Carro', 550, 'Transporte', 'Dinheiro', '2018-11-17'),
(29, 8, 'spotify', 26, 'Outros', 'CartÃ£o de CrÃ©dito', '2018-10-25'),
(30, 9, 'Faculdade', 1109.5, 'Estudos', 'Cheque', '2018-11-17'),
(31, 8, 'Putas', 3125, 'Lazer', 'Dinheiro', '2018-11-18');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contas_receber`
--

CREATE TABLE `contas_receber` (
  `id_contas_receber` int(11) NOT NULL,
  `id_usuarios` int(11) NOT NULL,
  `valor` float NOT NULL,
  `fonte_renda` varchar(255) DEFAULT NULL,
  `data` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `contas_receber`
--

INSERT INTO `contas_receber` (`id_contas_receber`, `id_usuarios`, `valor`, `fonte_renda`, `data`) VALUES
(2, 8, 1100, 'Trabalho Extras', '2018-10-31'),
(3, 8, 6500.15, 'Trabalho extras', '2018-11-01'),
(4, 8, 4300, 'Trabalho', '2018-12-01'),
(5, 9, 1700, 'Trabalho', '2018-11-01');

-- --------------------------------------------------------

--
-- Estrutura da tabela `emprestimos`
--

CREATE TABLE `emprestimos` (
  `id_emprestimos` int(11) NOT NULL,
  `id_usuarios` int(11) NOT NULL,
  `nome_investimento` char(150) DEFAULT NULL,
  `valor` float NOT NULL,
  `juros` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `investimentos`
--

CREATE TABLE `investimentos` (
  `id_investimentos` int(11) NOT NULL,
  `id_usuarios` int(11) NOT NULL,
  `nome_investimento` char(150) DEFAULT NULL,
  `valor` float NOT NULL,
  `juros` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `investimentos`
--

INSERT INTO `investimentos` (`id_investimentos`, `id_usuarios`, `nome_investimento`, `valor`, `juros`) VALUES
(6, 8, 'Tesouro Direto', 5600, 0.9);

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuarios` int(11) NOT NULL,
  `usuario` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `senha` varchar(10) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id_usuarios`, `usuario`, `email`, `senha`) VALUES
(9, 'Guilherme Bruno Montico', 'montico@logic.com', '1234'),
(8, 'Rafael Prazeres da Silva', 'rafael@logic.com', '123456');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contas_pagar`
--
ALTER TABLE `contas_pagar`
  ADD PRIMARY KEY (`id_contas_pagar`);

--
-- Indexes for table `contas_receber`
--
ALTER TABLE `contas_receber`
  ADD PRIMARY KEY (`id_contas_receber`);

--
-- Indexes for table `emprestimos`
--
ALTER TABLE `emprestimos`
  ADD PRIMARY KEY (`id_emprestimos`);

--
-- Indexes for table `investimentos`
--
ALTER TABLE `investimentos`
  ADD PRIMARY KEY (`id_investimentos`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuarios`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contas_pagar`
--
ALTER TABLE `contas_pagar`
  MODIFY `id_contas_pagar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `contas_receber`
--
ALTER TABLE `contas_receber`
  MODIFY `id_contas_receber` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `emprestimos`
--
ALTER TABLE `emprestimos`
  MODIFY `id_emprestimos` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `investimentos`
--
ALTER TABLE `investimentos`
  MODIFY `id_investimentos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuarios` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
